function d3AnimationEdges(EdgeInteractionInfo,heightsvg,widthsvg){
	// Animation is build from frames
	// Each transition of color has different states: old color, transition (can be multiple frames) and new color
	var width=widthsvg;
	var height=heightsvg;
	var encoder = new GIFEncoder(1250,1000);
	encoder.setRepeat(0);
	encoder.setDelay(2000);
	encoder.start()
	
	function getInteractionColors(position){
	var edgecolors=[];
	EdgeInfo=EdgeInteractionInfo
    for (i=0;i<EdgeInfo.length-1;i++){
		EdgeValue=Object.values(EdgeInfo[i])[position+1];
		if (EdgeValue==0){
			edgecolors.push(0);
			}
		if(EdgeValue==1){
			edgecolors.push(1)
			}
		}
		console.log(edgecolors)
	return edgecolors
	}
	
		function drawFrameWithNodeAndEdges(tp){
			 // Function to get the current frame for node coloring and edge interactions
	}
	
	function drawFrameEdgeInteractions(tp,RealTimePoints){
			// Function to draw the edge interaction without the node coloring
			if(RealTimePoints.includes(RealTimePoints[tp])==true){
				// Draw the current status of edge interactions
				edgecolorsmap=getInteractionColors(tp);
				d3.selectAll('line').style('opacity',function(d,i){return edgecolorsmap[i]})
				Frame=d3.selectAll('#network-area').node().innerHTML;
				return Frame
			}
			else{
				// Draw the transition state between the edge interactions
				// Check which edge is dissapearing between the two time points
				console.log('tussenframes moeten nog worden gemaakt')
			}
	}
	
	function drawCanvas(time_point,RealTimePoints){
		// get SVG frame
		SvgFrame=drawFrameEdgeInteractions(time_point,RealTimePoints); 
		// Convert SVG to Canvas
		var img= new Image();
		html = "<svg width=" +
            '"' + 1250+ '"' +
            " height=" + '"' + 1000 + '"' +
            ' version="1.1" xmlns="http://www.w3.org/2000/svg">' +
            SvgFrame +
            "</svg>"
        var imgsrc = 'data:image/svg+xml;base64,' + btoa(html);
		img.src = imgsrc;
		img.onload = function() {
					// create canvas to draw on
					var canvas=document.createElement('canvas')
					//document.body.appendchild(canvas)
					canvas.width=width;
					canvas.height=height;
					context=canvas.getContext('2d')
										context.fillStyle='#ffffff'
					context.fillRect(0,0,width,height)
					context.drawImage(img, 0, 0);
					var canvasdata = canvas.toDataURL("image/png");
					encoder.addFrame(context)
					if(time_point==RealTimePoints.length-1){
					showResults()
						}
					}		
	}
	
	
	function showResults(){
		console.log('animation finished')
		encoder.finish()
		DownloadName= $('#input-fileNameAnimation')[0].value
		encoder.download(DownloadName+"gif");
	}
	
	function createGifElement(){
		RealTimePoints=Object.keys(InteractionInfo[0])
		RealTimePoints.shift();
		console.log(RealTimePoints)
	for(timekeys=0;timekeys<RealTimePoints.length;timekeys++){
		// Get the canvas for each timeframe
		console.log(timekeys)
		console.log(encoder)
		drawCanvas(timekeys,RealTimePoints);

	}
		
	}
    createGifElement();
}
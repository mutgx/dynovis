// Create a histogram of data provided in format string + value
// R data frame is send to CreateHistogram in format  Gene Ontology term + occurence
Shiny.addCustomMessageHandler("CreateHistogram",
	function (message) {
		//dataset_GO=message
		var dataset={'GOterms':['id':'Glycolysis','occurence':5],['id':'Oxidative phosphorylation','occurence',9]}
		
		// set the size/dimensions of the plot
		var width=600;
		var height=300;
		
		// set the ranges of the plot
		// set the ranges of the x and y
		var x=d3.scalePoint().rangeRound([0,width]).domain(Object.keys(dataset.GOterms).length);
		var y=d3.scaleLinear().rangeRound([height,0]).domain([0,d3.max(dataset.Goterms.occurence)]);
		
		// Add the horizontal and vertical axis 
		var x_axis=d3.axisBottom(x);
		var y_axis=d3.axisLeft(y);
		
		// set the parameters for the hisogram (bins)
		var histogram = d3.histogram()
						  .value(function(d){return d.occurence})
						  .domain(x.domain());
		
		// add the svg to draw the histogram
		var svg=d3.select('#histogramGO').append('svg')
				  .attr('width',width)
				  .attr('height',height);
				  
		// Add the data to the plot
		// group the data for bars
		var bins=histogram(dataset_GO)
		// Scale the range of the y domain
		y.domain([0 d3.max(bins,function(d){return d.length})]);
		
		// add the bars to the plot
		svg.selectAll('rect')
		   .data(bins)
		   .enter().append('rect')
		   .attr('class','bar')
		   .attr('x',1)
		 
		
		// add the x Axis
		svg.append("g")
		   .attr("transform", "translate(0," + height + ")")
		   .call(x_axis);

		// add the y Axis
		svg.append("g")
           .call(y_axis);
      
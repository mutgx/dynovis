// Function that will create a HTML table
function createHTMLtable(dataframeobject,valuesToGet,id){
	if(id=='BiologicalData'){
	dataTableArray=[]
	ObjectKey=Object.keys(dataframeobject)[0]
	for(indices=0;indices<dataframeobject[ObjectKey].length;indices++){
		dataTableArray[indices]=[dataframeobject.Symbol[indices],
								 dataframeobject.GeneID[indices],
								 dataframeobject.Synonyms[indices],
								 dataframeobject.ReferenceID[indices],
								 dataframeobject.description[indices],
								 dataframeobject.genetype[indices],
								 dataframeobject.Pathway[indices],
								 dataframeobject.Disease[indices]]
	}
	
	columnnames=Object.keys(dataTableArray);
	datatest=dataTableArray

 $(function() {
        var testsTable =$('#DataTableDataAnalysis').dataTable({
	 bJQueryUI:true,
	 data:dataTableArray,
	 columns:[
	 {title:'Symbol'},
	 {title:'GeneID'},
	 {title:'Synonyms'},
	 {title:'ReferenceID'},
	 {title:'description'},
	 {title:'genetype'},
	 {title:'Pathway'},
	 {title:'Disease'}
		]
	})
	});	 
}
	if(id=='HUB'){
	dataTableArray=[]
	for(indices=0;indices<dataframeobject.length;indices++){
		dataTableArray[indices]=[dataframeobject[indices].id,
								 dataframeobject[indices].Degree,
								 dataframeobject[indices].inDegree,
								 dataframeobject[indices].outDegree,
		]
	}
	datatest=dataTableArray
	$(function() {
        var testsTable =$('#DataTableHUB').dataTable({
	 bJQueryUI:true,
	 data:dataTableArray,
	 columns:[
	 {title:'GeneID'},
	 {title:'TotalDegree'},
	 {title:'InnerDegree'},
	 {title:'OuterDegree'}
		]
	})
	});	
	}
	
	if(id=='DynamicHUB'){
	dataTableArray=[]
	for(indices=0;indices<dataframeobject.length;indices++){
		dataTableArray[indices]=[dataframeobject[indices][valuesToGet[0]],
								 dataframeobject[indices][valuesToGet[1]],
								 dataframeobject[indices][valuesToGet[2]],
								 dataframeobject[indices][valuesToGet[3]],
								 dataframeobject[indices][valuesToGet[4]],

		]
	}
	datatest=dataTableArray
	console.log(datatest)
	$(function() {
        var testsTable =$('#DataTableHUBDynamic').dataTable({
	 bJQueryUI:true,
	 data:dataTableArray,
	 columns:[
	 {title:'GeneID'},
	 {title:'Timepoint 1'},
	 {title:'Timepoint 2'},
	 {title:'Timepoint 3'},
	 {title:'Timepoint 4'},
		]
	})
	});	
	}

}
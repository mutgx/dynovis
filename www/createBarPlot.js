// Script to create a simple bar plot 
function createBarPlot(dataToPlot){

		var margins = [160, 160, 80, 80]; // margins  bottom top left right
		// set the size/dimensions of the plot
		var width=800-margins[2]-margins[3];
		var height=600-margins[0]-margins[1];
		
		// set the ranges of the plot
		// set the ranges of the x and y
		DataDT=dataToPlot.DT;
		DataDT=DataDT.slice(0,9);
		var x = d3.scaleBand().domain(DataDT.map(function(x){ return x; }))
    	.range([0, width])
    	.paddingInner([0.1])
	    .paddingOuter([0.3])
	    .align([0.5]);
		DataFreq=dataToPlot.Freq;
		DataFreq=DataFreq.slice(0,9);
		
		var y=d3.scaleLinear().domain([0,Math.max.apply(Math,DataFreq.map(function(x){return x}))+1]).range([height,0]);
		// Add the horizontal and vertical axis 
		var x_axis=d3.axisBottom(x).ticks(function(DataDT,i){return DataDT[i];});
		var y_axis=d3.axisLeft(y);
	
		
		var svg = d3.select("#PlotPathwayBars").append("svg")
					.attr("width", width + margins[2] + margins[3])
				    .attr("height", height + margins[1] + margins[0])
				    .append("g")
					.attr("transform", "translate(" + margins[2] + "," + 0 + ")");

		// add the x Axis
	    svg.append("g")
		   .attr("transform", "translate(0," + height + ")")
             .call(x_axis)
			.selectAll("text")	
            .style("text-anchor", "end")
            .attr("dx", "-.8em")
            .attr("dy", ".15em")
            .attr("transform", function(d) {
                return "rotate(-75)" 
                });
		// append the rectangles for the bar chart
	
		CombinedData=[]
		for(index=0;index<DataDT.length;index++){
		CombinedData[index]={id:DataDT[index],freq:DataFreq[index]}}
		
		svg.selectAll(".bar")
		   .data(CombinedData)
		   .enter().append("rect")
		   .attr("class", "bar")
           .attr("width", x.bandwidth())
		   .attr("height", function(CombinedData,i){return height-y(CombinedData.freq); })
	       .attr("x", function(CombinedData,i){return x(CombinedData.id); })
	       .attr("y", function(CombinedData,i){ return y(CombinedData.freq); }) 

		// add the y Axis
		svg.append("g")
		   .call(y_axis);
		   
		svg
		.append("text")
		.attr("transform", "translate(-35," +  (height+margins[0])/2 + ") rotate(-90)")
		.style("font-size","20px")
		.text("Occurence");
		

		
		
}
	
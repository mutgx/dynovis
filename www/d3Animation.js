function d3Animation(DataExpr,heightsvg,widthsvg,colors){
	// Animation is build from frames
	// Each transition of color has different states: old color, transition (can be multiple frames) and new color
	colors=colors
	var width=widthsvg;
	var height=heightsvg;
	var encoder = new GIFEncoder(1250,1000);
	encoder.setRepeat(0);
	encoder.setDelay(2000);
	encoder.start()
	
	

	function getNodeMap(position){  
	var colorScheme = colors;
    NodeData=dataExpr.ExprValue	
	var nodeMap=[];
	for(index=0;index<NodeData.length;index++){
			nodeMap.push(colorScheme(NodeData[index][Object.keys(NodeData[index])[position]]))};
	return nodeMap
	}
	
	
	function drawFrame(tp){
		colorsmap=getNodeMap(tp);
		GetStatusColoring=d3.selectAll('.node').attr('fill',function(d,i){return colorsmap[i]});
		//ColorEdgeWhite=d3.selectAll('.links').attr('stroke','white');
		Frame=d3.selectAll('#network-area').node().innerHTML;
		return Frame
	}
	

	
	// Function that will encode for the GIF animation
	function drawCanvas(time_point){
		// get SVG frame
		SvgFrame=drawFrame(time_point); 
		// Convert SVG to Canvas
		var img= new Image();
		html = "<svg width=" +
            '"' + 1250+ '"' +
            " height=" + '"' + 1000 + '"' +
            ' version="1.1" xmlns="http://www.w3.org/2000/svg">' +
            SvgFrame +
            "</svg>"
        var imgsrc = 'data:image/svg+xml;base64,' + btoa(html);
		img.src = imgsrc;
		img.onload = function() {
					// create canvas to draw on
					var canvas=document.createElement('canvas')
					//document.body.appendchild(canvas)
					canvas.width=width;
					canvas.height=height;
					context=canvas.getContext('2d')
					context.fillStyle='#ffffff'
					context.fillRect(0,0,width,height)
					context.drawImage(img, 0, 0);
					var canvasdata = canvas.toDataURL("image/png");
					encoder.addFrame(context)
					if(time_point==Object.keys(dataExpr.ExprValue[0]).length-1){
					showResults()
						}
					}		
	}

	function showResults(){
		console.log('animation finished')
		encoder.finish()
		DownloadName= $('#input-fileNameImage')[0].value
		encoder.download(DownloadName+"gif");
	}
	
	function createGifElement(){
	for(timekeys=0;timekeys<Object.keys(dataExpr.ExprValue[0]).length;timekeys++){
		// Get the canvas for each timeframe
		drawCanvas(timekeys);

	}
		
	}
	createGifElement()
}
function exportLineGraph2D(){
	var svgData = $('#PlotArea')[0].children[0].outerHTML;
	var svgBlob = new Blob([svgData], {type:"image/svg+xml;charset=utf-8"});
	var svgUrl = URL.createObjectURL(svgBlob);
	var downloadLink = document.createElement("a");
	downloadLink.href = svgUrl;
	downloadLink.download = $('#input-fileLineGraph').val();
	document.body.appendChild(downloadLink);
	downloadLink.click();
	document.body.removeChild(downloadLink);	
}


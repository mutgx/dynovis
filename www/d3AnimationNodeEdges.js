function d3AnimationNodeEdges(dataExpr,EdgeInteractionInfo,widthsvg,heightsvg){
// Animation is build from frames
	// Each transition of color has different states: old color, transition (can be multiple frames) and new color
	var width=widthsvg;
	var height=heightsvg;
	var encoder = new GIFEncoder(1250,1000);
	encoder.setRepeat(0);
	encoder.setDelay(2000);
	encoder.start()
	
	
	function getColor(valuelist){
	var colors=[];
	for(i=0;i<valuelist.length;i++){
		var hue=((1-valuelist[i])*120).toString(10)
		colors[i]=["hsl(",hue,",100%,50%)"].join("");}
	return colors;}
	
	function getNodeMap(position){
	var colorrange=getColor(d3.range(-3,1.4,0.1));  
	var colorScheme = d3.scaleLinear().domain([-2,0.5]).range(["#6AE817","#FFA200", "#B30409"]);
    NodeData=dataExpr.ExprValue	
	var nodeMap=[];
	for(index=0;index<NodeData.length;index++){
			nodeMap.push(colorScheme(NodeData[index][Object.keys(NodeData[index])[position]]))};
	return nodeMap
	}

	// Animation is build from frames
	// Each transition of color has different states: old color, transition (can be multiple frames) and new color
	var width=widthsvg;
	var height=heightsvg;
	var encoder = new GIFEncoder(1250,1000);
	encoder.setRepeat(0);
	encoder.setDelay(2000);
	encoder.start()
	
	function getInteractionColors(position){
	var edgecolors=[];
	EdgeInfo=EdgeInteractionInfo
    for (i=0;i<EdgeInfo.length-1;i++){
		EdgeValue=Object.values(EdgeInfo[i])[position+1];
		if (EdgeValue==0){
			edgecolors.push(0);
			}
		if(EdgeValue==1){
			edgecolors.push(1)
			}
		}
		console.log(edgecolors)
	return edgecolors
	}
	
	
	function drawFrameEdgeInteractions(tp){
			// Function to draw the edge interaction without the node coloring
			
				// Draw the current status of edge interactions
				colorsmap=getNodeMap(tp);
				edgecolorsmap=getInteractionColors(tp);
				GetStatusColoring=d3.selectAll('.node').attr('fill',function(d,i){return colorsmap[i]});
				
				UpdateStates=d3.selectAll('line').style('opacity',function(d,i){return edgecolorsmap[i]})
				Frame=d3.selectAll('#network-area').node().innerHTML;
				return Frame
			
	}
	
	// Function that will encode for the GIF animation
	function drawCanvas(time_point){
		// get SVG frame
		SvgFrame=drawFrameEdgeInteractions(time_point); 
		// Convert SVG to Canvas
		var img= new Image();
		html = "<svg width=" +
            '"' + 1250+ '"' +
            " height=" + '"' + 1000 + '"' +
            ' version="1.1" xmlns="http://www.w3.org/2000/svg">' +
            SvgFrame +
            "</svg>"
        var imgsrc = 'data:image/svg+xml;base64,' + btoa(html);
		img.src = imgsrc;
		img.onload = function() {
					// create canvas to draw on
					var canvas=document.createElement('canvas')
					canvas.width=width;
					canvas.height=height;
					context=canvas.getContext('2d')
					context.fillStyle='#ffffff'
					context.fillRect(0,0,width,height)
					context.drawImage(img, 0, 0);
					var canvasdata = canvas.toDataURL("image/png");

					encoder.addFrame(context)
					if(time_point==Object.keys(dataExpr.ExprValue[0]).length-1){
					showResults()
						}
					}		
	}

	function showResults(){
		console.log('animation finished')
		encoder.finish()
		DownloadName= $('#input-fileNameAnimation')[0].value
		encoder.download(DownloadName+"gif");
	}
	
	function createGifElement(){
	for(timekeys=0;timekeys<Object.keys(dataExpr.ExprValue[0]).length;timekeys++){
		// Get the canvas for each timeframe
		console.log(timekeys)
		console.log(encoder)
		drawCanvas(timekeys);

	}
		
	}
	createGifElement()









}
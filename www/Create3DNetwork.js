function Create3DNetwork(DataModel,DataExpr) {
		dataset=DataModel;
		timedata=DataExpr;

		var CAMERA_DISTANCE2NODES_FACTOR=100;
		var camera;
		var scene;
		var renderer;
		var mesh;
		var controls;
		var container;
		var clock = new THREE.Clock();
			 window.requestAnimFrame = (function(){
             return  window.requestAnimationFrame       || 
                      window.webkitRequestAnimationFrame || 
                      window.mozRequestAnimationFrame    || 
                      window.oRequestAnimationFrame      || 
                      window.msRequestAnimationFrame     || 
                      function( callback ){
                        window.setTimeout(callback, 1000 / 60);
                      };
            })();
		function initializeScene() {
//----------------------------------------------------------------------------------------------------------------------
// 									Set the scene and camera, add lights to the scene
//----------------------------------------------------------------------------------------------------------------------
		container = document.getElementById( '3Dnetworkvisualization' );
		scene = new THREE.Scene();
		scene.background = new THREE.Color( 0xffffff );
		camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 20000);
		renderer = new THREE.WebGLRenderer({antialias:true});
		renderer.setSize(800,700);
		// Add lights
		scene.add(new THREE.AmbientLight(0xbbbbbb));
		scene.add(new THREE.DirectionalLight(0xffffff, 0.3));

//----------------------------------------------------------------------------------------------------------------------
//									Construct the information space
//----------------------------------------------------------------------------------------------------------------------
	
	
	
//----------------------------------------------------------------------------------------------------------------------
//               					Construct the 3D network 
//----------------------------------------------------------------------------------------------------------------------
		const d3Nodes = [];
		for (let nodeId in dataset.nodes) { // Turn nodes into array
			const node = dataset.nodes[nodeId];
			node._id = nodeId;
			d3Nodes.push(node);
		}
		
		// simulation is runned to the end and not on tick ( so static simulation with only end of simulation shown)
		var simulation3D=d3.forceSimulation().nodes(d3Nodes)
				   .force('link',d3.forceLink().id(function(d) { return d.id; }).distance(5))
				   .force('charge',d3.forceManyBody())
				   .force('center',d3.forceCenter(0,0))
				   .force('x',d3.forceX(0))
				   .force('y',d3.forceY(0))
				   .stop();
				   
		// run the simulation
		for (var i = 0, n = Math.ceil(Math.log(simulation3D.alphaMin()) / Math.log(1 - simulation3D.alphaDecay())); i < n; ++i) {
		simulation3D.tick();}
		Zdimension=Array.apply(null, Array(d3Nodes.length)).map(function (x, i) { return (d3Nodes[i].y*0.8*Math.sin(d3Nodes[i].y*Math.PI)+d3Nodes[i].x*0.8*Math.sin(d3Nodes[i].y*Math.PI))})
		// Define the links
		const d3Links = dataset.links
		var sphereParent = new THREE.Object3D();
		d3Nodes.forEach(node => {
			const nodeMaterial = new THREE.MeshPhongMaterial({ color:'blue'});
			nodeMaterial.opacity = 0.75;
			const sphere = new THREE.Mesh(
			new THREE.SphereGeometry(5,32,32),nodeMaterial);
			sphere.position.x=node.x;
			sphere.position.y=node.y;
			sphere.position.z=Zdimension[node.index];
			sphere.name=d3Nodes.id;
			sphereParent.add(sphere)	
		});

		// push links into array
		NodeNames=[];
		for(x=0;x<dataset.nodes.length;x++){NodeNames.push(dataset.nodes[x].id)}
		d3Links.forEach(link => {
			const lineMaterial = new THREE.LineBasicMaterial({ color: 000000, transparent: true });		
			const line = new THREE.Line(new THREE.Geometry(), lineMaterial);
			line.geometry.vertices=[new THREE.Vector3(d3Nodes[NodeNames.indexOf(link.source.id)].x,d3Nodes[NodeNames.indexOf(link.source.id)].y,Zdimension[NodeNames.indexOf(link.source.id)]), new THREE.Vector3(d3Nodes[NodeNames.indexOf(link.target.id)].x,d3Nodes[NodeNames.indexOf(link.target.id)].y,Zdimension[NodeNames.indexOf(link.target.id)])];
			
			const StartName = link.source;
			const EndName = link.target;
			
			// Update the vector of the line
			
			sphereParent.add(line)
		});	
//----------------------------------------------------------------------------------------------------------------------
// 									Render the output
//----------------------------------------------------------------------------------------------------------------------
		container.appendChild( renderer.domElement );
		// Set Fly controls
	    controls = new THREE.FlyControls(camera, renderer.domElement);
		controls.movementSpeed = 100;
		controls.rollSpeed = 0.1;
		controls.dragToLook = true;
		camera.position.z =Math.cbrt(d3Nodes.length) * CAMERA_DISTANCE2NODES_FACTOR;
		scene.add(sphereParent); // Add the spheres to the scene
		//controls.addEventListener( 'change', render );
		window.addEventListener( 'resize', onWindowResize, false );
		render();
		animate();

		$('#TimeAnimation').click(function(){
      networkTimeSeries(sphereParent,1);
    });
	$('#PauseAnimation').click(function(){
      networkTimeSeries(sphereParent,2);
    });
	$('#ForwardFrame').click(function(){
      networkTimeSeries(sphereParent,3);
    });
	$('#BackwardFrame').click(function(){
      networkTimeSeries(sphereParent,4);
    });


		
		}
//--------------------------------------------------------------------------------------------------------------
//                          Animation funtion to visualize gene expression values on node
//----------------------------------------------------------------------------------------------------------------------
		// Search for the occurence of the node id in the time expression dataset
		var counter=0;
		function networkTimeSeries(SphereObjects,playerid){
			var ColorNodes3D=function(){
				if(counter==Object.keys(timedata.ExprValue[0]).length-1){

					colorsmap=getNodeMap(counter);
						for(nodeindex=0;nodeindex<dataset.nodes.length;nodeindex++){
						SphereObjects.children[nodeindex+86].material.opacity.set(0)
						SphereObjects.children[nodeindex].material.color.set(colorsmap[nodeindex])
						SphereObjects.children[nodeindex].material.needsUpdate=true;
						}
					
					clearInterval(runanimation3D)
						}
				else{
					colorsmap=getNodeMap(counter);
					for(nodeindex=0;nodeindex<dataset.nodes.length;nodeindex++){
					SphereObjects.children[nodeindex].material.color.set(colorsmap[nodeindex])
					SphereObjects.children[nodeindex].material.needsUpdate=true;
					var div=document.getElementById('TimeStep')
					
					}
					div.innerHTML=Object.keys(timedata.ExprValue[0])[counter]
					counter++;
						}
				//mesh.geometry.colorsNeedUpdate = true;
				}
				
				if(playerid==1){
					runanimation3D=setInterval(ColorNodes3D,2000);}
				if(playerid==2){
					//Handle the pause statement
					var elem=document.getElementById('PauseAnimation')
						if (elem.innerHTML=='Pause'){
						clearInterval(runanimation3D);
						elem.innerHTML='Resume';	
						// clear setInterval but save the time expired for resume function
						}
					else{
					elem.innerHTML='Pause';
					runanimation3D=setInterval(ColorNodes3D,2000);
					}		
				}
					
				if(playerid==3){
					
					// Forward frame with 1 time step
					counter=counter+1
					if(counter==Object.keys(timedata.ExprValue[0]).length){alert('max reached')}
					else{
					colorsmap=getNodeMap(counter);
					for(nodeindex=0;nodeindex<dataset.nodes.length;nodeindex++){
					SphereObjects.children[nodeindex].material.color.set(colorsmap[nodeindex])
					SphereObjects.children[nodeindex].material.needsUpdate=true;
					}}
					return counter
					
				}
				if(playerid==4){
				    // Backward frame with 1 time step
					counter=counter-1
					if(counter<0){alert('min reached');return counter=0}
					else{
					colorsmap=getNodeMap(counter);
					for(nodeindex=0;nodeindex<dataset.nodes.length;nodeindex++){
					SphereObjects.children[nodeindex].material.color.set(colorsmap[nodeindex])
					SphereObjects.children[nodeindex].material.needsUpdate=true;
					}}
					return counter
				}

		}
	
		function getNodeMap(position){
		var colorScheme3D = d3.scaleLinear().domain([0,1,0.05]).range(["red", "green"]);
		var nodeMap=[];
		for(nrindex=0;nrindex<dataset.nodes.length;nrindex++){
			nodeMap.push(colorScheme3D(timedata.ExprValue[nrindex][Object.keys(timedata.ExprValue[nrindex])[position]]))
			};
			return nodeMap
			}
  
//----------------------------------------------------------------------------------------------------------------
//									Define the animate, render and window resize functions
//----------------------------------------------------------------------------------------------------------------------
		
		// Standard functions for THREEjs behavior
		function animate() {
		//render();
		requestAnimationFrame( animate );
		var delta = clock.getDelta(); 
		controls.update(delta); 
		renderer.render( scene, camera );
		}
  
		function render() {
		
		}
		
		function onWindowResize() {
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize( window.innerWidth, window.innerHeight );
		render();
		}
		
		function __init__(){
				initializeScene();
				}
		
		__init__()

		$(document).ready(function(){
	$('#ExportImage3D').click(function(){
			requestAnimationFrame(render)
			renderer.render(scene,camera)
			var image3D=renderer.domElement.toDataURL('image/jpeg');
			imgNode=document.createElement('img');
			imgNode.src=image3D;
			document.body.appendChild(imgNode);
			
	})
})
}

		
	



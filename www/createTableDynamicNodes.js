function createTableDynamicNodes(EdgeInfoNodes){
	// Function to calculate the dynamic node degree
    DynamicDegree=[];
	for(index=1;index<=Object.keys(EdgeInfoNodes[0]).length-1;index++){

		var NodeOutgoingID=[];
		var NodeIncomingID=[];
	   edgecolorsmap=getInteractionColors(index,EdgeInfoNodes)
		// Color the stroke of each edge	
		Edges=d3.selectAll('.links').style('opacity',function(d,i){return edgecolorsmap[i];})
		Edges_OnlyPresent=Edges.filter(function(d,i){return Edges._groups[0][i].style.opacity==1});
		for(i=0;i<Edges_OnlyPresent._groups[0].length;i++){
			NodeOutgoingID[Edges_OnlyPresent._groups[0][i].__data__.target.index]=Edges_OnlyPresent._groups[0][i].__data__.target.id
			NodeIncomingID[Edges_OnlyPresent._groups[0][i].__data__.source.index]=Edges_OnlyPresent._groups[0][i].__data__.source.id	
		}
		var NodeOutgoingIDDegree=new Uint8Array(NodeOutgoingID.length);
		var NodeIncomingIDDegree=new Uint8Array(NodeIncomingID.length);
		for(j=0;j<NodeOutgoingID.length;j++){
            getDegree=Edges_OnlyPresent.filter(function(d,i){return Edges_OnlyPresent._groups[0][i].__data__.target.id==NodeOutgoingID[j]})
			NodeOutgoingIDDegree[j]=getDegree._groups[0].length
		}
		for(j=0;j<NodeOutgoingID.length;j++){
            getDegree=Edges_OnlyPresent.filter(function(d,i){return Edges_OnlyPresent._groups[0][i].__data__.source.id==NodeOutgoingID[j]})
			NodeIncomingIDDegree[j]=getDegree._groups[0].length
		    
		}
		NodeIncomingLinks2=[NodeIncomingID,NodeIncomingIDDegree];
		NodeOutgoingLinks2=[NodeOutgoingID,NodeOutgoingIDDegree];
		console.log(NodeIncomingLinks2)
		for(x=0;x<dataset.nodes.length;x++){
			// get the id of the node
			NodeID=dataset.nodes[x].id
			NodeID_DegreeIn=NodeIncomingLinks2[0].indexOf(NodeID)
			NodeID_DegreeOut=NodeOutgoingLinks2[0].indexOf(NodeID)
			
			if(NodeID_DegreeIn !=-1 && NodeID_DegreeOut ==-1){
			// add the node degrees as attributes to the dataset
			str='Degree '+Object.keys(EdgeInfo[0])[index]
			dataset.nodes[x][Object.keys(EdgeInfo[0])[index]]=NodeIncomingLinks2[1][NodeID_DegreeIn];}
			
			if(NodeID_DegreeIn ==-1 && NodeID_DegreeOut !=-1){
			// add the node degrees as attributes to the dataset
			str='Degree '+Object.keys(EdgeInfo[0])[index]
			dataset.nodes[x][Object.keys(EdgeInfo[0])[index]]=NodeIncomingLinks2[1][NodeID_DegreeOut];}
			
			if(NodeID_DegreeIn !=-1 && NodeID_DegreeOut !=-1){
			// add the node degrees as attributes to the dataset
			str='Degree '+Object.keys(EdgeInfo[0])[index]
			dataset.nodes[x][Object.keys(EdgeInfo[0])[index]]=NodeIncomingLinks2[1][NodeID_DegreeIn]+NodeOutgoingLinks2[1][NodeID_DegreeOut];}
			else{
				str='Degree '+Object.keys(EdgeInfo[0])[index]
			    dataset.nodes[x][Object.keys(EdgeInfo[0])[index]]=0;
			}
			}
	}
	return dataset.nodes
}
	
	function getInteractionColors(position,EdgeInfoNodes){
	var edgecolors=[];
	EdgeInfo=EdgeInfoNodes
    for (i=0;i<EdgeInfo.length-1;i++){
		EdgeValue=Object.values(EdgeInfo[i])[position];
		if (EdgeValue==0){
			edgecolors.push(0);
			}
		if(EdgeValue==1){
			edgecolors.push(1)
			}
		}
	return edgecolors
	}
Shiny.addCustomMessageHandler('InitializeNetworkVisualization',
 function(message){
	 // Message can consist of different data variables, including network data, time/dose/time+dose series data
	 // Check the dimensions of the message to adjust for dynamic message size
	 dataAll=message
	 dataEntryKeys=Object.keys(dataAll)  // labels to call different functions
	 BuildNetwork=0
	
	// Set the loader screen to block display
	
	 
	 // Call the function to create the network visualization
	 for(index=0;index<dataEntryKeys.length;index++){
	 if(BuildNetwork==0){
	 if(dataEntryKeys[index]=='DataNetwork'){
		 // Send Network information and ExprValue information for animation 
		 Create2DNetwork(dataAll.DataNetwork,dataAll.DataExpr,dataAll.EdgeInfo,dataAll.NodeDB,dataAll.pathwaysToPlot,dataAll.RangePoint)
		 BuildNetwork=1
	 }}
	 }
 }) // end of the shiny.addCustomMessageHandler
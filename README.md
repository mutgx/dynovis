# DynOVis

Repository for the visualization tool DynOVis.

Repository for TGX visualization tool to visualizize dose-over-time effects on network graphs.

In this project the main objective is to obtain more insight in the biological processes underlying the exposure of a cell model to a (toxic) substance. These biological processes are often dynamic, and thus their activity should be monitored at multiple time points.

To run the DynOVis tool, download this repository to your local disk and run 'server.R' in Rstudio. Please see the manual for the full details of the tool and the installation requirements.

We recommend to use Firefox browser for the most optimal experience of the visualization tool.

# R libraries for DynOVis
DynOVis requires a couple of R packages to be installed:R shiny,Xlsx,rJava,igraph,BioNet